# Windows 10

## Installation media creation

### Download Windows ISO

In order to create Windows bootable media you need to get the Windows ISO. This can be done by using official  [Windows Media Creation tool](https://www.microsoft.com/en-us/software-download/windows10)

### Burn Image

You can use the same Windows Media Creation tool to either only download the image or even write the image on to your USB Flash drive.

Also you can use [Rufus](https://rufus.ie/en/) to write ISO on your USB Flash drive

### autounattend.xml

If you will use you USB Flash drive to boot Windows Installation you can simply take on of the autounattend.xml files. Pick UEFI or MBR according to your installation type and rename the file to autounattend.xml. Pleace in in the root of your USB Flash drive that is allready imaged with Windows installation.

You can also create your own autounattend.xml using this [windowsafg](https://www.windowsafg.com/) site.

In case you want to create an ISO with autounattend.xml to use it in VM's follow this easy procedure.

- [ ] Install [ImgBurn](https://www.imgburn.com/index.php?act=download)
- [ ] Extract Windows ISO into a folder (e.g. %USERPROFILE%\Desktop\Windows10)
- [ ] Copy the autounattend.xml to %USERPROFILE%\Desktop\Windows10
- [ ] Open CMD and call ImgBurn with following command (lets assume Extracted ISO is in %USERPROFILE%\Desktop\Windows10 )
```
"C:\Program Files (x86)\ImgBurn\ImgBurn.exe" /mode build /buildinputmode advanced /buildoutputmode imagefile /src "%USERPROFILE%\Desktop\Windows10" /dest "%USERPROFILE%\Desktop\WIN10_autounattend.iso" /volumelabel "WIN10" /filesystem "UDF" /udfrevision "1.02" /recursesubdirectories yes /includehiddenfiles yes /includesystemfiles yes /bootimage "%USERPROFILE%\Desktop\Windows10\boot\etfsboot.com" /bootemutype 0 /bootsectorstoload 8 /bootloadsegment 07C0 /closesuccess /rootfolder yes /portable /noimagedetails
```

The created ISO can be used to in virtual machines or can be later imaged to a USB Flash drive via Windows Media Creation tool, Rufus or ImgBurn itself.

**Important: please examine the given as example autounattend.xml Both of them are wiping the disk drive and creating a new partition. In case you don't need this either manually adjust the xml file or create a new file via [windowsafg](https://www.windowsafg.com/)**

## Post installation configuration

In the folder **2_configuration** you can find useful scripts/registry files/group policies to help with post-installation configuration.